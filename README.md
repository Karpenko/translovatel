# Translovatel

_Translovatel_ is a simple script which aggregates translations into up to 5 languages and
displays them. It uses [googletrans](https://pypi.org/project/googletrans/) and [word2word](https://pypi.org/project/word2word/) libraries to translate words. It also prints out diffs if translation given by _googletrans_ was not included in the list of translations given by _word2word_. 

List of languages is now limited to \[ua, ru, cz, no, en, fr], but is easily extendable. 

Helpers include frequency lists for the languages in question. 

# Installation

check for the newest version [on testPyPi](https://test.pypi.org/project/translovatel/) and run the command given there, 
for instance, 
>https://test.pypi.org/project/translovatel/  

In case installation packages of other packages, try to install them first and rerun installation command.

# Usage

>from translovatel import translovatel  
>translovatel.translate()

After the command is run, you will be asked for a language to translate from, and for a list of languages to translate
to. After you language choices are set, you will be asked to type a word you want to translate.
