"""Module containing strings and functions for cli, frequency lists for languages."""

import json
import os
import site


FROM_STRING = 'What language do you want to translate from? \
               ["ua", "ru", "cz", "no", "en", "fr"]: '
TO_STRING = 'What languages do you want to translate to? \
    ["ua", "ru", "cz", "no", "en", "fr"]: '


def get_path(file_name):
    """Form a path for frequency lists."""
    file_name = os.path.join(os.path.relpath(__package__), file_name)
    path = site.getsitepackages()[0]
    file_path = os.path.join(path, file_name)
    return file_path


def create_lists(lang_file):
    """Access docs containing frequency lists and convert them to lists."""
    with open(lang_file, "r") as l_file:
        line = l_file.read()
        language_set = line.split("\n")
    return language_set


def choose_languages():
    """Choose languages from available list."""
    while True:

        language_from = input(FROM_STRING)
        language_to = [i.strip() for i in input(TO_STRING).split(",")]
        if language_from in language_to:
            print("Traslation from the language to itself is not supported")
        else:
            return (language_from, language_to)


LANG_KEYS = ["fr", "ru", "uk", "no", "cs"]

en_gloss = json.load(open(get_path("data.json")))
ENGLISH_DT = list(en_gloss.keys())


MOST_COMMON = {"en": ENGLISH_DT}
for lang in LANG_KEYS:
    FILENAME = lang + "_frequent.txt"
    MOST_COMMON[lang] = create_lists(get_path(FILENAME))
