from setuptools import setup, find_packages

with open("README.md", "r") as readme_file:
    readme = readme_file.read()

requirements = ["googletrans==3.0.0", "word2word==1.0.0", "requests>=2"]

setup(
    name="translovatel",
    version="0.0.15",
    author="Hanna Karpenko",
    author_email="hannakarpe@gmail.com",
    description="A package to translate simultaneously",
    long_description=readme,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/Karpenko/translovatel",
    packages=["translovatel"],
    package_data={"translovatel": ["data.json", "en_frequent.txt", "fr_frequent.txt", "ru_frequent.txt", "uk_frequent.txt", "cs_frequent.txt", "no_frequent.txt"]},
    py_modules=["translovatel", "helpers"],
    include_package_data=True,
    install_requires=requirements,
    classifiers=[
        "Programming Language :: Python :: 3.7",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
