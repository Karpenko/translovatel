"""This is translovatel modul.

It allows to translate a word up to the 5 languages simultaneously,
using word2word and googletrans Python libraries.


It also compares googletrans and w2w translations, and shows those google
translations which are not in word_2_word translations.


List of languages is [ua,ru,cz,no,en,fr], but it is easily extendable.
"""

import logging
import sys

from difflib import get_close_matches
from pprint import pprint
from googletrans import Translator
from word2word import Word2word

from translovatel.helpers import MOST_COMMON, choose_languages


CODE_DICT = {"en": "en", "fr": "fr", "ru": "ru", "no": "no", "ua": "uk", "cz": "cs"}
LOGGER = logging.Logger(name="logger", level="DEBUG")
SEPARATOR = "\n\n**************************\n\n"
NL = "\n"
INITIAL_QUESTION = "What word are you looking for? (Type N for exit)"


def translate():
    """Trigger the translator.

    First, you will be asked for languages to translate to and from.
    Then, loop asking for words to translate and translating them will execute.
    """
    return __main()


def __main():
    """Pick languages to translate to/from and start the translation loop."""
    language_from, languages_to = choose_languages()
    print(SEPARATOR, "Languages you chose: ", language_from, languages_to, SEPARATOR)

    while True:
        search_word = input(INITIAL_QUESTION).lower().strip()
        if search_word == "n":
            sys.exit()

        __word_2_word(search_word, {}, language_from, languages_to)


def __construct_translators(language_from, languages_to):
    """Construct w2w translators for chosen languages."""
    translators = []

    for language in languages_to:
        translator = Word2word(CODE_DICT[language_from], CODE_DICT[language])
        translators.append(translator)
    return translators


def __word_2_word(search_word, results_dict, language_from, languages_to):
    """Try to translate with w2w, google translate, and compare the results.

    param: search_word - word to be translated
    param: results_dict - dict with translations from
           w2w and google for given word
    """
    results = []
    translators = __construct_translators(language_from, languages_to)
    results_dict = __initiate_results_dict(search_word, results_dict)

    __google_translate(search_word, results_dict, language_from, languages_to)

    # Try to translate first pair, if KeyError check for mistakes
    try:
        translation = translators[0](search_word)
    except KeyError:
        commons = __check_for_mistakes(search_word, translators[0].lang1)
        for word in commons:
            if word != search_word:
                answer = input(f"Sorry, not found. Maybe, you meant {word}? (Y/N) ")
                if answer.upper() == "Y" and word not in results_dict:
                    return __word_2_word(word, results_dict, language_from, languages_to)
    else:
        results.append(translation)

    # Translate to other languages (without cheks for mistakes)
    if len(translators) > 1:
        results = __aggregate_results(translators[1:], results, search_word)

    # assign values to results_dict, and call compare function
    results_dict[search_word]["W2W"] = results
    return __compare(results_dict)


def __google_translate(search_word, results_dict, language_from, languages_to):
    """Save translated with googletrans search_word to results_dict."""
    results = []
    translator = Translator()
    results_dict = __initiate_results_dict(search_word, results_dict)

    for language in languages_to:
        try:
            translation = translator.translate(
                search_word, dest=CODE_DICT[language], src=CODE_DICT[language_from]
            )
        except AttributeError:
            results.append(
                f"Google Translate was not able to translate word"
                f' "{search_word}" into {language}'
            )
        else:
            results.append(translation.text)

    results_dict[search_word]["Google"] = results


def __aggregate_results(translators, results, search_word):
    for translator in translators:
        try:
            translation = translator(search_word)
        except KeyError:
            results.append(
                f"There is no translation in Open Subtitles"
                f'for "{search_word}" into {translator.lang2}'
            )
        else:
            results.append(translation)

    return results


def __compare(results_dict):
    """Print results_dict (dict with translations), calculate diffs based on it."""
    diffs = []

    print(SEPARATOR)
    pprint(results_dict)
    for i in results_dict.keys():
        w2w = results_dict[i]["W2W"]
        google = results_dict[i]["Google"]
        if not w2w:
            diffs = []
        else:
            for j in google:
                if "was not able" not in j and isinstance(w2w[0], list):
                    if j not in w2w[google.index(j)]:
                        diffs.append(j)

    print(SEPARATOR, "The diffs are like: ", diffs)

    return results_dict, diffs


def __check_for_mistakes(search_word, language_file):
    """Check for similar words to search_word."""
    common_words = MOST_COMMON[language_file]

    return get_close_matches(search_word, common_words, n=7, cutoff=0.7)


def __initiate_results_dict(search_word, results_dict):
    """Assign default keys to results_dict."""
    if results_dict == {}:
        results_dict = {search_word: {"W2W": [], "Google": []}}
    if search_word not in results_dict.keys():
        results_dict[search_word] = {"W2W": [], "Google": []}

    return results_dict


if __name__ == "__main__":
    __main()
